var Theme = {};

(function ($) {
    'use strict';

    /**
     * Constructor
     */
    Theme.common = function () {
        this.elements = {
            htmlBody: $('html, body'),
            body: $('body')
        };

        return this;
    };

    /**
     * Methods
     */
    Theme.common.prototype = {
        load: function (className, options) {
            if (Theme[className] !== undefined) {
                return new Theme[className](this, options);
            } else {
                console.error('Class "Theme.' + className + '" not found.');
            }
        },
        autoload: function (options) {
            var self = this;

            if (options !== undefined) {
                $.each(options, function (className, condition) {
                    if (self.elements.body.is(condition)) {
                        return self.load(className).init();
                    }
                });

            } else {
                console.error('Options is undefined.');
            }
        }
    };

    /**
     * OnReady
     */
    $(document).ready(function () {
        var theme = new Theme.common();

        // All
        theme.load('all').init();
    });

})(jQuery);