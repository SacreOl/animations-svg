(function ($) {
    'use strict';

    Theme.all = function (common, options) {

        // Héritage
        this.common = common;

        // Éléments
        this.elements = {
            slider: $('#slider-main')
        };

        this.deviceDetect = new $.DeviceDetect();
        this.devices = this.deviceDetect.getDevices();
    };

    Theme.all.prototype = {
        init: function () {
            this.sliderHandler();
            this.initFancybox();
        },


        sliderHandler: function () {
            var self = this;

            if (!self.devices.desktop) {
                self.elements.slider.addClass('owl-carousel');
                self.elements.slider.owlCarousel({
                    items: 1,
                    loop: true
                });
            }

        },


        initFancybox: function () {
            $.fancybox.defaults.closeExisting = true;
            $.fancybox.defaults.protect = true;
            // $.fancybox.defaults.modal = true;
            $.fancybox.defaults.animationEffect = "zoom-in-out";  // zoom, fade, zoom-in-out
            // $.fancybox.defaults.animationDuration = 366;  // en ms
            $.fancybox.defaults.baseClass = 'fancybox--animation';
            $.fancybox.defaults.btnTpl.smallBtn =
                '<button type="button" data-fancybox-close class="btn btn--icon icon icon--close l-secondary l-circle" title="{{CLOSE}}">' +
                // '<svg xmlns="http://www.w3.org/2000/svg" version="1" viewBox="0 0 24 24"><path d="M13 12l5-5-1-1-5 5-5-5-1 1 5 5-5 5 1 1 5-5 5 5 1-1z"/></svg>' +
                "</button>";

            // Focus handling
            // ==============

            // Try to focus on the first focusable element after opening
            // $.fancybox.defaults.autoFocus = true;
            // Put focus back to active element after closing
            // $.fancybox.defaults.backFocus = true;
            // Do not let user to focus on element outside modal content
            // $.fancybox.defaults.trapFocus = true;

            // Module specific options
            // =======================
            $.fancybox.defaults.touch = false;
        }

    };

})(jQuery);