# Animations SVG

Un dépôt de test pour les animations SVG.


```
lando composer          Runs composer commands
lando db-export [file]  Exports database from a service into a file
lando db-import <file>  Imports a dump file into database service
lando mysql|psql        Drops into a MySQL (or psql) shell on a database service
lando php               Runs php commands
```


**Download a dependency with composer**
```
lando composer require phpunit/phpunit --dev
```

**Importer une base de donnée**
```
lando db-import dump.sql.gz
```

**Drop into a mysql shell**
```
lando mysql
```

**Check hte app's installed php extensions**
```
lando php -m
```