const package = require('./package.json');
const gulp = require('gulp');
const { series } = require('gulp');
const { watch } = require('gulp');
const $ = require('gulp-load-plugins')({
    pattern: ['*'],
    scope: ['devDependencies']
});
const async = require("async");
const timestamp = Math.round(Date.now() / 1000);

// Debug task
// ------------------------------------------------------------------------------------

function debug(cb) {
    console.log('\n');
    console.log(package);
    console.log('\n');
    cb();
}


// Suppression du dossier BUILD (dist)
// ------------------------------------------------------------------------------------

function clean() {
    return $.del([package.paths.dist.folder + '/**/*']).then(paths => {
        console.log('Dossiers et fichiers supprimés :\n', paths.join('\n'));
    });
}


// Copie des fonts du dossier src au dossier dist
// ------------------------------------------------------------------------------------

function fonts() {
    return gulp.src(package.paths.src.fonts + '/**/*.{ttf,svg,eot,woff,woff2}')
        .pipe(gulp.dest(package.paths.dist.fonts));
}


// Génération de la font-icon
// ------------------------------------------------------------------------------------

function fonticon(done) {
    var iconStream = gulp.src([package.paths.src.icons + '/**/*.svg'])
        .pipe($.iconfont({
            fontName: package.iconfontname,
            formats: ['woff', 'woff2', 'svg'],
            timestamp: timestamp
        }));

    async.parallel([
        function handleGlyphs (cb) {
            iconStream.on('glyphs', function(glyphs, options) {
                gulp.src(package.paths.src.icons + '/template.css')
                    .pipe($.consolidate('lodash', {
                        glyphs: glyphs,
                        fontName: package.iconfontname,
                        fontVersion: timestamp,
                        fontPath: '../fonts/icons/',
                        className: 'icon'
                    }))
                    .pipe($.rename(function (path) {
                        path.basename = "_icon";
                        path.extname = ".scss";
                    }))
                    .pipe(gulp.dest(package.paths.src.styles + '/global'))
                    .on('finish', cb);
            });
        },
        function handleFonts (cb) {
            iconStream
                .pipe(gulp.dest(package.paths.src.fonts + '/icons'))
                .on('finish', cb);
        }
    ], done);
}


// Copie des libs CSS
// ------------------------------------------------------------------------------------

function css() {
    return gulp.src(package.paths.src.styles + '/**/*.css')
        .pipe($.sourcemaps.init())
        .pipe($.cleanCss({debug: true}, (details) => {
            console.log(`${details.name}: ${details.stats.originalSize}`);
            console.log(`${details.name}: ${details.stats.minifiedSize}`);
        }))
        .pipe($.sourcemaps.write())
        .pipe(gulp.dest(package.paths.dist.css));
}


// Compilation des fichiers SASS
// Minification des fichiers SASS
// Auto-prefixage des classes propriétaires
// Génération de fichiers sourcemaps
// ------------------------------------------------------------------------------------

function sass() {
    return gulp.src(package.paths.src.styles + '/**/*.scss')
        .pipe($.sourcemaps.init({loadMaps: true}))
        .pipe($.sass.sync({
            outputStyle: 'compressed'
        }).on('error', $.sass.logError))
        .pipe($.autoprefixer())
        .pipe($.sourcemaps.write('.'))
        .pipe(gulp.dest(package.paths.dist.css));
}


// Optimisation & compression des images
// Copie dans le dossier dist
// ------------------------------------------------------------------------------------
function images() {
    return gulp.src(package.paths.src.images + '/**/*.{png,jpg,gif,svg,xml,webmanifest,ico}')
        .pipe($.imagemin([
            $.imagemin.gifsicle({interlaced: true}),
            $.imagemin.jpegtran({progressive: true}),
            $.imagemin.optipng({optimizationLevel: 7}),
            $.imagemin.svgo({
                plugins: [
                    {removeViewBox: true},
                    {collapseGroups: false},
                    {cleanupIDs: false}
                ]
            })
        ]))
        .pipe(gulp.dest(package.paths.dist.images));
}



// Watcher
// ------------------------------------------------------------------------------------
function watcher() {
    watch(package.paths.src.styles + '/**/*.css', css);
    watch(package.paths.src.styles + '/**/*.scss', sass);
    watch(package.paths.src.images + '/**/*.{png,jpg,gif,svg,xml,webmanifest,ico}', images);
    watch(package.paths.src.fonts + '/**/*.{ttf,svg,eot,woff,woff2}', fonts);
}








/*
 * You can use CommonJS `exports` module notation to declare tasks
 */
exports.debug = debug;
exports.clean = clean;
exports.fonts = fonts;
exports.css = css;
exports.fonticon = series(fonticon, fonts);
exports.sass = sass;
exports.styles = series(css, sass);
exports.images = images;

exports.watch = watcher;
exports.default = series(clean, fonticon, fonts, css, sass, images);