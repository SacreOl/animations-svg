<?php
$items = [
        'form-validation',
        'form-validation',
        'form-validation'
];
?>
<ul class="list list--primary">

    <?php foreach ($items as $dir => $item): ?>
        <li class="list-item l-third">
            <article class="item-wrapper">
                <header class="item-header">
                    <h2 class="item-title"><?php echo $item; ?></h2>
                </header>
                <div class="item-content">
                    <div class="svg svg--<?php echo $item; ?>">
                        <div class="circle"></div>
                        <div class="cross"></div>
                    </div>
                </div>
            </article>
        </li>
    <?php endforeach; ?>

</ul>