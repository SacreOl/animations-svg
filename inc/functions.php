<?php

function dsm($var) {
    echo '<pre><code>';
    print_r($var);
    echo '</code></pre>';
}

/**
 * @param $path
 * @return false|string
 */
function getSvg($path) {
    return file_get_contents('dist/img/svg/'.$path.'.svg');
}