<?php include_once 'inc/functions.php'; ?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8"/>
    <meta name="robots" content="noindex, nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no, maximum-scale=1"/>

    <link rel="apple-touch-icon" sizes="180x180" href="dist/img/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="dist/img/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="dist/img/favicons/favicon-16x16.png">
    <link rel="manifest" href="dist/img/favicons/site.webmanifest">
    <link rel="mask-icon" href="dist/img/favicons/safari-pinned-tab.svg" color="#8fd117">
    <meta name="apple-mobile-web-app-title" content="Animations SVG">
    <meta name="application-name" content="Animations SVG">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">

    <link rel="stylesheet" href="dist/css/libs/jquery.fancybox.min.css">
    <link rel="stylesheet" href="dist/css/libs/owl.carousel.css">
    <link rel="stylesheet" href="dist/css/libs/animate.min.css">
    <link rel="stylesheet" href="dist/css/theme.css">
    <title>Animations SVG</title>
</head>
<body>
    <main class="main" role="main">
        <div class="row">
            <header class="content-header">
                <h1 class="content-title">Animations SVG</h1>
            </header>
            <?php include_once 'templates/partials/list.php'; ?>
        </div>
    </main>

<script src="js/vendor/jquery-3.3.1.min.js"></script>
<script src="js/vendor/devicedetect.min.js"></script>
<script src="js/vendor/jquery.fancybox.min.js"></script>
<script src="js/vendor/owl.carousel.min.js"></script>
<script src="js/theme.js"></script>
<script src="js/theme/all.js"></script>
</body>
</html>